<img src="assets/bb-logo-square.png" height="128"/>

# Discord Logging Bot

> Powerful and complete replacement for Discord's built-in audit log.

**Discord Logging Bot _A.K.A "Big Brother"_** is a configurable Discord bot that logs events in a Discord server to a channel. It is intended to be used as a more powerful alternative to Discord's built-in audit log.

## Usage

You have two options for using this bot:

1. **RECOMMENDED:** Clone this repository and run it yourself using Docker or something like PM2.
2. Invite the public instance (A.K.A. "Big Brother") to your server by using this link: https://discord.com/api/oauth2/authorize?client_id=1124895077785944144&permissions=67210288&scope=bot%20applications.commands

### Configuration

None yet, but it's coming soon™!

## Development

### TODO List

- [x] Basic gateway-to-channel logging (messages, reactions, etc.)
- [ ] Logging filters (initially via environment variables)
- [ ] Allow configuration via slash commands for ease of use
- [ ] Prettier logging output (e.g. embeds)
- [ ] Alerting on matches (e.g. via mention, email, SMS, etc.)
- [x] Better development experience (e.g. Docker Compose, CI/CD publishing, etc.)
