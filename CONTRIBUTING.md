# Contributing

It's pretty simple! Here's the process:

1. Fork the repo
2. Make your changes
3. Submit a merge request
4. Assign me (`@zuedev`) as a reviewer
5. ???
6. Profit!

I don't have any specific guidelines for contributing, but I do have a few suggestions:

- Follow the existing code style as much as possible. I use Prettier to format the code, so you can use that to format your code before submitting a merge request.
- Keep your changes as small as possible. If you're adding a new feature, try to break it up into multiple commits. This makes it easier for me to review your changes.
- Ensure your changes as focused as possible. If you're fixing a bug, try to only fix that bug. If you're adding a new feature, try to only add that feature.
