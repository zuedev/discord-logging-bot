# Privacy Policy

1. Introduction

Thank you for choosing to use Discord Logging Bot (A.K.A. "Big Brother"). This privacy policy outlines our practices regarding the collection, use, and disclosure of information that we receive through Discord Logging Bot (A.K.A. "Big Brother"). This privacy policy applies to all users of Discord Logging Bot (A.K.A. "Big Brother").

2. Information Collection and Use

As Discord Logging Bot (A.K.A. "Big Brother") is a Discord bot, all data collection, use, and storage is handled by Discord, Inc. We do not collect or store any personal data independently. For information about how Discord, Inc. collects and uses your information, please refer to [Discord's Privacy Policy](https://discord.com/privacy).

3. Changes to this Privacy Policy

We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page. You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.

4. Contact Us

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact me at [zuedev@gmail.com](mailto:zuedev@gmail.com) or on [Discord](https://discord.gg/UvgJgkREQa).
